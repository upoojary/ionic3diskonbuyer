import { Component,OnInit,ViewChild } from '@angular/core';
import { Platform,NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';
import { LocationPage } from '../pages/location/location';
import { DiscountHomePage } from '../pages/discount-home/discount-home';

import { AuthService } from '../services/auth.service';
import { LoadingService } from '../services/loading.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  rootPage:any  ;
  @ViewChild('navPage') navPage: NavController;

  signupPage:any = SignupPage;
  signinPage:any = SigninPage;
  locationPage:any = LocationPage;
  isAuthenticated:boolean = false;
  discountHomePage:any = DiscountHomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
          private authService: AuthService,private loadingService: LoadingService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  ngOnInit(){
    this.authService.authStatusChanged.subscribe(
      (authenticated) => {
        console.log(" New Auth "+authenticated);
        this.isAuthenticated = authenticated;
        if (authenticated) {
            console.log("Calling from app component");
            

            this.navPage.setRoot(this.discountHomePage);
        } else {
            this.navPage.setRoot(this.signinPage);
        }
      }
    );
    this.authService.initAuth();
  }

  onLoadPage(page:any):void {
    this.navPage.setRoot(page);
  }

  logoutUser():void {
    this.authService.logout();
  }
}

