import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';

import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';
import { TabsPage } from '../pages/tabs/tabs';
import { LocationPage } from '../pages/location/location';
import { DiscountHomePage } from '../pages/discount-home/discount-home';
import { FilterdiscountPage } from '../pages/filterdiscount/filterdiscount';

import { AuthService } from '../services/auth.service';
import { LoadingService } from '../services/loading.service';
import { ToastService } from '../services/toast.service';
import { LocationService } from '../services/location.service';

import { MyApp } from './app.component';

@NgModule({
  declarations: [
    MyApp,
    SignupPage,
    SigninPage,
    TabsPage,
    LocationPage,
    DiscountHomePage,
    FilterdiscountPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyCwCUSTwD8gKH4y0eyl64ge90QSgaKAyaE'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SignupPage,
    SigninPage,
    TabsPage,
    LocationPage,
    DiscountHomePage,
    FilterdiscountPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    LoadingService,
    ToastService,
    LocationService,
    Geolocation,
    NativeGeocoder
  ]
})
export class AppModule {}
