import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastService {
	
	toastObj:any = null;

	constructor(private toastController:ToastController ){}

	createToast(message:string):void {
		this.toastObj = this.toastController.create({
				message: message,
  				showCloseButton: true,
  				position:'middle',
      			closeButtonText: 'Ok'
		});

		this.toastObj.present();
	}


}