import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class LoadingService {

	constructor(private loadingController: LoadingController){}

	public loadingModal:any = null;

	createLoader(isLoading:boolean):void {
		if(isLoading)
		{
			console.log("******************************   Createddddd");
			this.loadingModal = this.loadingController.create({
			content: 'Loading ...',
			});
			this.loadingModal.present();
		}
		
	}

	dismissLoader():void {
		if(this.loadingModal)
		{
			console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$   DIsmissssss");
			this.loadingModal.dismiss();
			this.loadingModal = null;
		}
	}

	checkIsLoading():boolean {
		if(this.loadingModal)
		{
			console.log(" sending true");
			return true;
		}
		console.log(" sending true");
		return false;
	}
	
}