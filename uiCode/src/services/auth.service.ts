import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { LoadingController } from 'ionic-angular';

import {
  CognitoUserPool,
  CognitoUserAttribute,
  CognitoUser,
  AuthenticationDetails,
  CognitoUserSession
} from 'amazon-cognito-identity-js';

import { User } from '../models/user.model';

const POOL_DATA = {
  UserPoolId: 'ap-south-1_8yMGqFtsX',
  ClientId: '6jv18noafa1hp4413n53uadp09'
};

const userPool = new CognitoUserPool(POOL_DATA);

@Injectable()
export class AuthService {

  authIsLoading = new BehaviorSubject<boolean>(false);
  authDidFail = new BehaviorSubject<boolean>(false);
  isConfirmed = new BehaviorSubject<boolean>(false);
  newUserCreated = new BehaviorSubject<boolean>(false);
  authStatusChanged = new Subject<boolean>();
  registeredUser: CognitoUser;

  constructor(private loadingController: LoadingController){}

  signUp(username: string, email: string, password: string): void {

     let loadingModal = this.loadingController.create({
      content: 'Loading ...',
      });
      loadingModal.present();

        const user: User = {
      username: username,
      email: email,
      password: password
      };

    const attrList: CognitoUserAttribute[] = [];
    const emailAttribute = {
      Name: 'email',
      Value: user.email
    };
    attrList.push(new CognitoUserAttribute(emailAttribute));
    userPool.signUp(user.username, user.password, attrList, null, (err, result) => {
      if (err) {
        loadingModal.dismiss();
        this.authDidFail.next(true);
        return;
      }
      loadingModal.dismiss();
      this.authDidFail.next(false);
      this.newUserCreated.next(true);
      this.registeredUser = result.user;
    });
    return;
  }

  confirmUser(username: string, code: string) {
  let loadingModal = this.loadingController.create({
      content: 'Loading ...',
      });
      loadingModal.present();

    const userData = {
      Username: username,
      Pool: userPool
    };
    const cognitUser = new CognitoUser(userData);
    cognitUser.confirmRegistration(code, true, (err, result) => {
      if (err) {
        loadingModal.dismiss();
        this.authDidFail.next(true);
        return;
      }
      loadingModal.dismiss();
      this.authDidFail.next(false);
      this.isConfirmed.next(true);
      
    });
  }

  signIn(username: string, password: string): void {
    const authData = {
      Username: username,
      Password: password
    };
    const authDetails = new AuthenticationDetails(authData);
    const userData = {
      Username: username,
      Pool: userPool
    };

    let loadingModal = this.loadingController.create({
      content: 'Loading ...',
      });
      loadingModal.present();

    const cognitoUser = new CognitoUser(userData);
    const that = this;
    cognitoUser.authenticateUser(authDetails, {
      onSuccess (result: CognitoUserSession) {
        loadingModal.dismiss();
        that.authStatusChanged.next(true);
        that.authDidFail.next(false);
        console.log(result+"*****");
      },
      onFailure(err) {
        that.authDidFail.next(true);
        console.log(err+"!!!!!!");
      }
    });
    //this.authStatusChanged.next(true); // create user with cognito data
    return;
  }

  getAuthenticatedUser() {
    return userPool.getCurrentUser();
  }

  logout() {
    this.getAuthenticatedUser().signOut();
    this.authStatusChanged.next(false);
  }

  isAuthenticated(): Observable<boolean> {
    const user = this.getAuthenticatedUser();
    const obs = Observable.create((observer) => {
      if (!user) {
        observer.next(false);
      } else {
        user.getSession((err, session) => {
          if (err) {
            observer.next(false);
          } else {
            if (session.isValid()) {
              observer.next(true);
            } else {
              observer.next(false);
            }
          }
        });
      }
      observer.complete();
    });
    return obs;
  }

  initAuth() {
    this.isAuthenticated().subscribe(
      (auth) => this.authStatusChanged.next(auth)
    );
  }

}