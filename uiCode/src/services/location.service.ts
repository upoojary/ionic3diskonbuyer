import { Injectable } from '@angular/core';
import { Location } from '../models/location.model';


import { Subject } from 'rxjs/Subject';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LocationService {
	currentLocation:Location = null;
	isLocationSetSubject = new BehaviorSubject<boolean>(false);

	setCurrentLocation(newLocation:Location): void {
		this.currentLocation = newLocation;
		this.isLocationSetSubject.next(true);
	}

	getCurrentLocation():Location {
		return this.currentLocation;
	}
	
}