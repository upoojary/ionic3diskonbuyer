import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the FilterdiscountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filterdiscount',
  templateUrl: 'filterdiscount.html',
})
export class FilterdiscountPage implements OnInit {

	existingFilters:{categoryName: string, isSelected: boolean}[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewController:ViewController) {
  }

  closeFilterModal():void {
  		
  		this.viewController.dismiss();
  }

  ngOnInit() {

  	this.filterCategories.push(
  		{'categoryName':'food','isSelected':false},
  		{'categoryName':'sports','isSelected':false},
  		{'categoryName':'lifestyle','isSelected':false},
  		{'categoryName':'fashion','isSelected':false},
  		{'categoryName':'groceries','isSelected':false}
  	);

  	this.existingFilters = this.navParams.get('filteredArray');

  	console.log("Existing Filtered Array "+JSON.stringify(this.existingFilters));

  	for(var i =0 ; i< this.filterCategories.length;i++)
  	{
  		for(var j=0; j< this.existingFilters.length ;j++)
  		{
  			if(this.filterCategories[i].categoryName === this.existingFilters[j].categoryName)
  			{
  				this.filterCategories[i].isSelected = true;
  			}
  		}
  	}
  }

  filterCategories:{categoryName: string , isSelected: boolean}[] = [];

  applyFilter():void {
  	let appliedFilter :{categoryName: string, isSelected: boolean}[] = [];

  	for(var i=0;i<this.filterCategories.length;i++)
  	{
  		if(this.filterCategories[i].isSelected)
  		{
  			appliedFilter.push(this.filterCategories[i]);
  		}
  	}

  	this.viewController.dismiss(appliedFilter);
  }

}
