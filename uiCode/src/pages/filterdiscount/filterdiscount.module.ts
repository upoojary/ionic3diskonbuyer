import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FilterdiscountPage } from './filterdiscount';

@NgModule({
  declarations: [
    FilterdiscountPage,
  ],
  imports: [
    IonicPageModule.forChild(FilterdiscountPage),
  ],
})
export class FilterdiscountPageModule {}
