import { Component,OnInit,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,ToastController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage implements OnInit {
	  confirmUser = false;
	  didFail = false;
	  isLoading = false;
	  loadingUI = null;
	  isLoadingCreated = false;
	  useremail:string = null;

  	 failedToasted = null;

	@ViewChild('signUpForm') form: NgForm;
	@ViewChild('confirmUserForm') confirmForm: NgForm;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  				private loadingController: LoadingController,private authService:AuthService,
  				private toast:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  ngOnInit(){

  	this.authService.authIsLoading.subscribe(
  		(isLoading:boolean) => {
  			console.log(" Is it Loading? "+isLoading)
  			this.isLoading = isLoading;

  			if(this.isLoading)
  			{
  				this.isLoadingCreated = true;
  				this.loadingUI = this.loadingController.create({
  								content: 'Creating your profile !!!',
  								});
  				this.loadingUI.present();
  			}
  			else
  			{
  				if(this.isLoadingCreated)
  				{
  					this.loadingUI.dismiss();
  					this.isLoadingCreated = false;
  				}
  				
  			}
  		}
  	);

  	this.authService.authDidFail.subscribe(
  		(didFail:boolean) => {
  			
  			this.didFail = didFail;
  			if(this.didFail)
  			{
  				console.log(" Some thing failed try again");

  				this.failedToasted = this.toast.create({
  				message: 'Something went wrong, Please try again!!!!',
  				showCloseButton: true,
  				position:'middle',
      			closeButtonText: 'Ok'
  				});
  				
  				this.failedToasted.present();
  			}
  			
  		}
  	);

  	this.authService.newUserCreated.subscribe(
  		(isNewUser:boolean) => {
  			if(isNewUser)
  			{
  				const newUserToast = this.toast.create({
  				message: 'Verifiction code sent to '+this.useremail,
  				showCloseButton: true,
  				position:'top',
      			closeButtonText: 'Ok'
  				});
  				newUserToast.present();
  			}
  			
  		}
  	);

  	this.authService.isConfirmed.subscribe(
  		(isConfirm:boolean) => {
  			const conf = isConfirm;
  			if(conf)
  			{
  				console.log("Is confirmed");
  				//this.form.reset();
  				//this.confirmForm.reset();
  			}
  		}
  	);
  }

  onClickSignUp():void {
  	const userName = this.form.value.userName;
  	const password = this.form.value.password;
  	const email = this.form.value.email;
  	this.useremail = this.form.value.email;
  	this.authService.signUp(userName,email,password);
  }

  onConfirm(formValue : {'confirmUserName':string,'code':string}): void {
  		this.authService.confirmUser(formValue.confirmUserName, formValue.code);
  }

}
