import { Component,ViewChild,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { LoadingService } from '../../services/loading.service';
import { SignupPage } from '../signup/signup';



@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage implements OnInit {

	@ViewChild('signInForm') form:NgForm;
	signupPage:any = SignupPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  				private authService:AuthService,private loadingService:LoadingService,private toastController:ToastController ) {  }

  ngOnInit(){
  		this.authService.authDidFail.subscribe(
  			(hasFailed:boolean) => {
  				console.log("Sign Has Failed ? "+hasFailed);
  				if(hasFailed)
  				{
  					const toastObj = this.toastController.create({
						message: 'Something went wrong, try again!!!',
		  				showCloseButton: true,
		  				position:'middle',
		      			closeButtonText: 'Ok'
						});

					toastObj.present();
  				}
  			}
  		)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }

  onSignIn():void {
  	const username = this.form.value.username;
  	const password = this.form.value.password;
    console.log("Calling from sign in component");
    

  	this.authService.signIn(username,password);
  }

  newUser():void {
  		this.navCtrl.push(this.signupPage);
  }

}
