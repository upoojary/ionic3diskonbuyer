import { Component,OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { LocationService } from '../../services/location.service';
import { LoadingService } from '../../services/loading.service';

import { Location } from '../../models/location.model';
import { Discount } from '../../models/discount.model';
import Discounts from '../../models/discounts.temp';
import { FilterdiscountPage } from '../filterdiscount/filterdiscount';

@IonicPage()
@Component({
  selector: 'page-discount-home',
  templateUrl: 'discount-home.html',
})
export class DiscountHomePage implements OnInit {

	user:string = "Ullas Poojary";
	discuounts:Discount[] = Discounts;
	fullDiscounts:Discount[] = Discounts;
	isFilterApplied: boolean = false;
	location:Location = null;
	currentLocation: string = '';
	filteredArray:{categoryName: string , isSelected: boolean}[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
  		private filterModalController: ModalController, private locationService: LocationService,
  		private geoLocation:Geolocation,private nativeGeocoder: NativeGeocoder) {
  }

  ngOnInit(){

  	this.geoLocation.getCurrentPosition()
  		.then(
  				(location:any)=> {
  						this.location = new Location(location.coords.lat,location.coords.lng);
  						this.location.lat = location.coords.latitude;
  						this.location.long = location.coords.longitude;
  						this.locationService.setCurrentLocation(this.location);
  						console.log(location);
  						}
  			)
  		.catch(
  				error => {console.log(error);}
  			);

  		this.locationService.isLocationSetSubject.subscribe((isLocationSet:boolean) => {
  							if(isLocationSet)
  							{
  								console.log("New Location Set "+JSON.stringify(this.locationService.getCurrentLocation()));
  								let currLoc: Location = this.locationService.getCurrentLocation();
  								this.nativeGeocoder.reverseGeocode(currLoc.lat, currLoc.long)
								  .then((result: NativeGeocoderReverseResult) => {
								  									console.log(JSON.stringify(result));
								  									this.currentLocation = result.subLocality;
								  							})
								  .catch((error: any) => console.log(error));
							}
  					})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiscountHomePage');
	    
  }

  ionViewDidEnter(){

  }

  openFilterModel():void {
  		//resetting the filter
  		this.discuounts = this.fullDiscounts;

  		let filterModal = this.filterModalController.create(FilterdiscountPage,{filteredArray:this.filteredArray});
  		filterModal.present();

  		filterModal.onDidDismiss( (data:any)=> {
  				console.log(data);
  				if(data && data.length > 0)
  				{
  					this.isFilterApplied = true;
  					this.filteredArray = data;
  					var filterString = "";

  					for(var i=0;i<data.length;i++)
  					{
  						filterString = filterString+" "+data[i].categoryName;
  					}
  					console.log("Filter String "+filterString);

  					this.discuounts = this.discuounts.filter((discount: Discount) => {
  						console.log(JSON.stringify(discount));
  						console.log("******************* "+discount.category+" "+discount.category.toLowerCase().indexOf(filterString.toLowerCase()));
  						return filterString.toLowerCase().indexOf(discount.category.toLowerCase()) != -1;
  					});

  				}
  				else
  				{
  					this.isFilterApplied = false;
  					this.filteredArray = [];
  					this.discuounts = this.fullDiscounts;	
  				}
  		});	
  }

  onSelectCategory(selectedCategory: string):void {
  	this.discuounts = this.fullDiscounts;
  	if(selectedCategory.toLowerCase() != 'all')
  	{
  		this.discuounts = this.discuounts.filter( (discount: Discount) => 
  			{
  				console.log(selectedCategory+" "+JSON.stringify(discount));
  				return discount.category.toLowerCase().indexOf(selectedCategory) != -1;
  			}
  	 )
  	}
  	
  }

}
