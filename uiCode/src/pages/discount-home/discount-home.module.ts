import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiscountHomePage } from './discount-home';

@NgModule({
  declarations: [
    DiscountHomePage,
  ],
  imports: [
    IonicPageModule.forChild(DiscountHomePage),
  ],
})
export class DiscountHomePageModule {}
