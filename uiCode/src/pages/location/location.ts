import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Location } from '../../models/location.model';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the LocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage implements OnInit {

	location:Location = null;
	setLocation:Location ;
	myLocation:Location;
  constructor(public navCtrl: NavController, public navParams: NavParams,private geoLocation:Geolocation) {
  }

  ngOnInit(){
  	this.geoLocation.getCurrentPosition()
  		.then(
  			(location:any)=> {
  						this.location = new Location(location.coords.lat,location.coords.lng);
  						this.location.lat = location.coords.latitude;
  						this.location.long = location.coords.longitude;
  						console.log(location);
  						}
  		)
  		.catch(
  				error => {console.log(error);}
  			)

  		/*this.geoLocation.watchPosition()
  				.subscribe((position:any) => {
  					console.log(position.coords.longitude + '  position ' + position.coords.latitude);
  					this.location = new Location(position.coords.latitude,position.coords.longitude);
  					
  				});*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
  }

  onSetMarker(event:any):void {
  	this.setLocation = new Location(event.coords.lat,event.coords.lng);
  }

}
