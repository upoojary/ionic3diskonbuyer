export class Discount {
	
	constructor(public imgUrl: string,public shopName: string, public shopLocation: string ,public discountTitle: string, public discountDescription: string,public category: string){}

}